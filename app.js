"use strict";
define([
         'angular',
         'uiRouter',
         'ngAnimate'
       ], function (angular) {

    var app = angular.module('app', ['ui.router', 'ngAnimate']);

    app.init = function () {
      angular.bootstrap(document, ['app']);
    };

    app.config(function($stateProvider) {

      var states = [
        { 
          name: 'default', 
          url: '',
          templateUrl: "partials/about.html",
          controller: 'AboutController as about'
        },
        { 
          name: 'about', 
          url: '/',
          templateUrl: "partials/about.html",
          controller: 'AboutController as about'
        },
        { 
          name: 'country', 
          url: '/country',
          views: {
            "": {
              templateUrl: "partials/country.html",
              controller: 'CountryController as country'
            },
            "somevalue@country": {
              controller: 'CountryController as country',
              templateUrl: "partials/somevalue.view.html",
            },
          },
        },
        { 
          name: 'page', 
          url: '/page/*path', 
          views: {
            "": {
              templateUrl: "partials/page.html",
              controller: 'PageController as page'
            },
            "view@page": {
              controller: 'PageController as page',
              templateUrl: "partials/page.view.html",
            },
            "somevalue@page": {
              controller: 'PageController as hello',
              templateUrl: "partials/somevalue.view.html",
            },
         },
        },
        { 
          name: 'view', 
          url: '/page/*path', 
          views: {
            "view@page": {
                controller: 'PageController as page',
                templateUrl: "partials/page.view.html",
            },
            "somevalue@page": {
                controller: 'PageController as hello',
                templateUrl: "partials/somevalue.view.html",
              },
          },
        },
      ];
      states.forEach(function(state) {
        $stateProvider.state(state);
      });
    });

    //   a very simple store service purely to store one value that can be shared among all controllers

    app.factory('store', function() {
      var store = {
        someValue: null
      };
       store.saveSomeValue = function(value){
        store.someValue = value;
      };
       return store;
    });

    return app;
});