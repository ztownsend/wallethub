"use strict";
require.config({
    baseUrl: "",    
    paths: {
        'angular': 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular.min',
        'uiRouter': 'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.3.1/angular-ui-router.min',
        'ngAnimate': 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular-animate.min'
    },
    shim: { 
        angular: {
          exports : 'angular'
        },
        'uiRouter': {
            deps: ['angular']
        },
        'ngAnimate': {
            deps: ['angular']
        }
    },
    deps: ['app']
});

require(['app', 'services/countries', 'controllers/countryController', 'controllers/aboutController', 'controllers/pageController'], function (app) {
  app.init();
});