"use strict";
define(['app'], function(app) {

  // This is a separate controller which accesses the store.someValue, which can be saved from any controller

  app.controller('AboutController', function($stateParams, store) {   
    this.valueToBeSaved  = store.someValue || '';
    this.someValueStored = store.someValue;
    this.val1 ='';
    this.val2 ='';
    this.val3 ='';
    this.max1 ='';
    this.max2 ='';
    this.max3 ='';
    this.total = 0;
    this.phone = '';
    this.money = '';

    this.saveValue = function() {
      store.saveSomeValue(this.valueToBeSaved);
      this.someValueStored = store.someValue;
    };
  });


  app.directive('phoneInput', function($browser) {
      return {
          restrict: "A",
          link: function($scope, $element) {
              var listener = function() {
                  var tel = $element.val().replace(/[^0-9]/g, '');
                  tel = formatTel(tel);
                  $element.val(tel);
              };
              $element.bind('change', listener);
              $element.bind('keydown', function() {
                  $browser.defer(listener); // Have to do this or changes don't get picked up properly
              });
              function formatTel(tel) {
                if (!tel) { return ''; }
                var value = tel.toString().trim().replace(/^\+/, '');
                if (value.match(/[^0-9]/)) {
                    return tel;
                }
                var city, number;
                switch (value.length) {
                    case 1:
                    case 2:
                    case 3:
                        city = value;
                        break;
                    default:
                        city = value.slice(0, 3);
                        number = value.slice(3);
                }
                if(number){
                    if(number.length>3){
                        number = number.slice(0, 3) + '-' + number.slice(3,7);
                    }
                    else{
                        number = number;
                    }
                    return ("(" + city + ") " + number).trim();
                }
                else{
                    return "(" + city;
                }
            }
          }
      };
  });


  app.directive('moneyInput', function($browser) {
      return {
          restrict: "A",
          link: function($scope, $element) {
              var listener = function() {
                  var money = $element.val().replace(/[^0-9]/g, '');
                  money = removeLeadingZeros(money);
                  money = formatDollarsWithCommas(money);
                  $element.val('$' + money);
              };

              $element.bind('change', listener);
              $element.bind('keydown', function() {
                  $browser.defer(listener);
              });

              function removeLeadingZeros(val) {
                  return val.substr(0,1) == '0' ? val.substring(1) : val;
              }

              function formatDollarsWithCommas(dollars) {
                return dollars.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              }
          }
      };
  });


  app.directive('sumInput', function($browser) {
      return {
          require: 'ngModel',
          restrict: "A",
          scope: false,
          controller: 'AboutController',
          controllerAs: 'about',

          link: function($scope, $element, $attrs, ngModelCtrl) {
              var listener = function() {
                  $element.val($element.val().replace(/[^0-9/.]/g, ''));
                  var totalCalculated = Number($scope.about.val1) + Number($scope.about.val2) + Number($scope.about.val3);
                  if($element[0].id == 'total') {
                    if($scope.about.total !== '' && $scope.about.total !== 0) {
                      var totalEntered = Number($scope.about.total) ;
                      var totalDiff = totalEntered / totalCalculated;
                      $scope.about.val1 = (Number($scope.about.val1) ) * totalDiff;
                      $scope.about.val2 = (Number($scope.about.val2) ) * totalDiff;
                      $scope.about.val3 = (Number($scope.about.val3) ) * totalDiff;
                    }
                  } else {
                   $scope.about.total = totalCalculated;
                 }
                 $scope.about.totalEntered = totalCalculated;
                 $scope.$apply();
              };

              ngModelCtrl.$parsers.push(function(viewValue) {
                  return viewValue.replace(/[^0-9/.]/g, '');
              });

              ngModelCtrl.$render = function() {
                 if (ngModelCtrl.$viewValue.replace) $element.val(ngModelCtrl.$viewValue.replace(/[^0-9/.]/g, ''));
              };

              $element.bind('change', listener);
              $element.bind('keydown', function() {
                  $browser.defer(listener); // Forces change on every keypress
              });
          }
      };
  });


  app.directive('maxInput', function() {
      return {
          restrict: "A",
          scope: false,
          controller: 'AboutController',
          controllerAs: 'about',
      
          link: function($scope, $element) {
              var listener = function() {
                  var fieldLength = $element[0].value.length;
                  if(fieldLength==5 || fieldLength==6) {  //  including fieldLength of 6 provides better behavior when coming back from next element
                    $element[0].value = $element[0].value.substring(0,5);
                    var nextinput = $element.next('input');
                    if (nextinput.length === 1) {
                      nextinput[0].focus();
                    }
                  } else if(fieldLength===0) {
                    var previousinput = $element[0].previousElementSibling;
                    if (previousinput) {
                      previousinput.focus();
                    } 
                  } else {
                    $element[0].value = $element[0].value.substring(0,5);
                  }
              };
              $element.bind('keyup', listener);
          }
      };
  });
});