"use strict";
define(['app'], function(app) {

  //  This controller is shared by multiple pages and is designed to prove the url parameters and nested views

  app.controller('PageController', function($stateParams, store) {
    var path = $stateParams.path;
    var paths = path.split("\/");
    this.pageId = paths[0];
    if(paths[1]) {
      this.viewId = paths[1];
    }
    this.valueToBeSaved  = store.someValue || '';
    this.someValueStored = store.someValue;

    this.saveValue = function() {
      store.saveSomeValue(this.valueToBeSaved);
      this.someValueStored = store.someValue;
    };
  });
});