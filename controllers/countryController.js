"use strict";
define(['app'], function(app) {

  // This is a separate controller which accesses the store.someValue, which can be saved from any controller

  app.controller('CountryController', function($stateParams, store, CountriesService) {

    this.valueToBeSaved  = store.someValue || '';
    this.someValueStored = store.someValue;
    var self = this;
    this.order = 'capital';
      
    CountriesService.getAllCountries().then(function(countries) {
      self.countries = countries;
    });

    this.sortBy = function(order) {
      this.reverse = (order !== null && this.order === order) ? !this.reverse : false;
      this.order = order;
    };

    this.filterByName = function(element) {
      if(self.filterName) {
        var rg = new RegExp('^' + self.filterName + '','i');
        return element.name.match(rg) ? true : false;
      } else {
        return element;
      }
    };

    this.saveValue = function() {
      store.saveSomeValue(this.valueToBeSaved);
      this.someValueStored = store.someValue;
    };
  });
});