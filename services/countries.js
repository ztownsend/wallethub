"use strict";
define(['app'], function(app) {

  app.service('CountriesService', function($http) {
    var service = {
      getAllCountries: function() {
        return $http.get('data/countries.json', { cache: true }).then(function(resp) {
          return resp.data;
        }, function(error) {
             console.log(error);
        });
      },
      
      getPerson: function(id) {
        function personMatchesParam(person) {
          return person.id === id;
        }
        
        return service.getAllPeople().then(function (people) {
          return people.find(personMatchesParam);
        });
      }
    };
    return service;
  });
});